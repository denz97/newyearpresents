package src;

abstract class Shipping {
    abstract void presentShipping();

    public void shippingType(boolean shippingAcrossContinents) {
        if (shippingAcrossContinents == true) {
            System.out.println("present will be shipped by plane");
        } else {
            System.out.println("present will be shipped by train");
        }
    }
}
