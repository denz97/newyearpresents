package src;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Shipping shipping = new Shipping() {
            @Override
            void presentShipping() {
                boolean shippingNeeded = true;
                if (shippingNeeded == true ) {
                    shippingType(true);
                } else {
                    System.out.println("Present without shipping");
                }
            }
        };
        Action action = new Action();
        action.presentConfig("red");
        // System.out.println(BoxColors.blue);
        BoxСontent cont1 = new BoxСontent() {
            @Override
            public void presentForMan() {
                System.out.println(BoxColors.green);
                System.out.print(" Socks for men ");

            }

            @Override
            public void presentForWomen() {
                System.out.println(BoxColors.red);
                System.out.print(" lunch boxes for women ");
            }

        };
        cont1.presentForMan();
        cont1.presentForWomen();
        shipping.presentShipping();

        mapDemo();

    }

    public static void mapDemo() {
        Map<String, Integer> fruitCalories = new HashMap<>();
        fruitCalories.put("apple", 95);
        fruitCalories.put("lemon", 20);
        fruitCalories.put("orange", 45);
        for (Map.Entry<String, Integer> entry : fruitCalories.entrySet()) {
            System.out.println(entry.getValue());
        }
        System.out.println("---------------");
        fruitCalories.forEach(
                (k, v) -> System.out.println("Fruit:" + k + "Calories:" + v));

    }

}
